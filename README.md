
# Babbel Automation Challenge


## Overview
This problem is designed to test the candidates comfort with cross-platform automation framework  

## Instructions
 Choose your favourite website and implement one or two automated tests. Create a fully functioning framework with it. It can be very simple but it has to be functioning
 Also implement automated tests (one or two test cases) on android app. The build for android app is provided in the repository.
  

## Deliverables
- Working automation framework with test cases on your choice of website. 
- Working automation framework with test cases on provided android build.
- A writeup/README describing your solutions, tools, and assumptions made.  Be as thorough as possible with your explanation.

Once complete, please upload your solution on github and send link to the solutions to <insert email here> 

**PLEASE NOTE, We encourage to keep your git history :) **

Good luck, if you have any questions please mail <email address here>

